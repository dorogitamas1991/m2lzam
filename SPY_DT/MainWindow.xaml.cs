﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SPY_DT
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        GameLogic VM;

        DispatcherTimer dt;
        
        public MainWindow()
        {
            InitializeComponent();
        }
        int hanyadik;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            /*Window1 menu = new Window1();
            App.Current.MainWindow = menu;
            menu.Show();
            this.Close();*/
            hanyadik = 1;
            VM = new GameLogic("LVL1.txt", new Size(jatekter.ActualWidth, jatekter.ActualHeight));
            jatekter.Init(VM);

            dt = new DispatcherTimer();
            dt.Interval = TimeSpan.FromMilliseconds(20);
            dt.Tick += Dt_Tick;
            dt.Start();

            
            


        }
       
        int szamlaloforguard =0;
        private void Dt_Tick(object sender, EventArgs e)
        {
            jatekter.InvalidateVisual();
            VM.Gravity();
            
           
                foreach (Guard item in VM.Guards)
                {

                if (szamlaloforguard <= 600 || szamlaloforguard >1000 && szamlaloforguard<=1600 )
                {
                    szamlaloforguard++;
                }
                else if (szamlaloforguard >600 && szamlaloforguard<=1000)
                {
                    item.MozgasJobbra();
                    szamlaloforguard++;
                }
                else if (szamlaloforguard > 1600 && szamlaloforguard <= 2000 )
                {
                    item.MozgasBalra();
                    szamlaloforguard++;
                }
                else { szamlaloforguard = 1; }
                }

            foreach (Guard item in VM.Guards)
            {
                VM.Guardlo();
                if (item.Lo == 1)
                {
                    item.Lovedek.MozgasJobb();
                   
                }
                else if (item.Lo == 2)
                {
                    item.Lovedek.MozgasBal();
                   


                }

                foreach (Wall item2 in VM.Walls)
                {
                    Rect exitrect = new Rect(item2.Place.X, item2.Place.Y, item.Width, item.Height);
                   Rect playerrect = new Rect(VM.Player.Place.X, VM.Player.Place.Y, VM.Player.Width, VM.Player.Height);
                    if (item2.Miféle == 6)
                    {
                        if (playerrect.IntersectsWith(exitrect))
                        {
                            if (hanyadik == 1)
                            {
                                VM = new GameLogic("LVL2.txt", new Size(jatekter.ActualWidth, jatekter.ActualHeight));
                                jatekter.Init(VM);
                                hanyadik++;
                            }
                            else if (hanyadik == 2)
                            {
                                VM = new GameLogic("LVL3.txt", new Size(jatekter.ActualWidth, jatekter.ActualHeight));
                                jatekter.Init(VM);
                                hanyadik++;
                            }
                            else if (hanyadik == 3)
                            {
                                MessageBox.Show("Gratulálok, végigvitted a SPY-t");
                                VM = new GameLogic("LVL1.txt", new Size(jatekter.ActualWidth, jatekter.ActualHeight));
                                jatekter.Init(VM);
                                hanyadik = 1;

                            }
                        }
                    }
                }
               
               
            }

            if (VM.Player.Alive == false)
            {
                MessageBox.Show("Vesztettél");
                VM = new GameLogic("LVL1.txt", new Size(jatekter.ActualWidth, jatekter.ActualHeight));
                jatekter.Init(VM);

            }





        }
      

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            VM = new GameLogic("LVL1.txt", new Size(jatekter.ActualWidth, jatekter.ActualHeight));
            jatekter.Init(VM);
        }

       

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Right)
            {
                VM.Movetimerjobb.Start();
            }
            if (e.Key == Key.Left)
            {
                VM.Movetimerbal.Start();
            }

            if (e.Key == Key.Up)
            {
                VM.Jumptimer.Start();
            }
            if (e.Key == Key.Space)
            {
                VM.Lovedekek.Add(new Pisztoly(VM.Player.Place));

               
                VM.Lovestimer1.Start();
            }
           
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Right || e.Key==Key.Left)
            {
                VM.Movetimerbal.Stop();
                VM.Movetimerjobb.Stop();
            }
        }
    }
}
