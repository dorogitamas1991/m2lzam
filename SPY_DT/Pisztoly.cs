﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SPY_DT
{
    class Pisztoly
    {
        Point helyzet;
        int height;
        int width;
       
        

        public Pisztoly(Point helyzet)
        {
            this.helyzet = helyzet;
            width = 30;
            height = 2;
            
        }
        public void MozgasJobb()
        { Point newcenter = new Point(helyzet.X + 50, helyzet.Y);
            helyzet = newcenter;
        }
        public void MozgasBal()
        {
            Point newcenter = new Point(helyzet.X - 50, helyzet.Y);
            helyzet = newcenter;
        }
        public Point Helyzet { get => helyzet; set => helyzet = value; }
        public int Height { get => height; set => height = value; }
        public int Width { get => width; set => width = value; }
        
    }
}
