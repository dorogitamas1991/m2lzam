﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SPY_DT
{
    class Guard
    {
        Point place;
        int width;
        int height;
        Size jatekter;
        Pisztoly lovedek;
        bool merre;
        int lo;

        public Guard()
        {
            lo = 0;
        }

        public Point Place { get => place; set => place = value; }
        public int Width { get => width; set => width = value; }
        public int Height { get => height; set => height = value; }
        public Size Jatekter { get => jatekter; set => jatekter = value; }
        internal Pisztoly Lovedek { get => lovedek; set => lovedek = value; }
        public bool Merre { get => merre; set => merre = value; }
        public int Lo { get => lo; set => lo = value; }

        public void MozgasJobbra()
        {
            merre = true;
            Point newpoint = new Point(place.X -1 , place.Y);
            place = newpoint;

        }
        public void MozgasBalra()
        {
            merre = false;
            Point newpoint = new Point(place.X + 1, place.Y);
            place = newpoint;
        }
        public int Visszalo(Character jatekos,List<Wall> wallist)
        {
            
            if (jatekos.Place.X<=place.X && jatekos.Place.X >= place.X-500 && merre ==true && jatekos.Place.Y<=place.Y+10 && jatekos.Place.Y<=place.Y+10)
            {
                Rect lovedekrect = new Rect(lovedek.Helyzet.X, lovedek.Helyzet.Y, 30, 2);

                Rect jatekosrect = new Rect(jatekos.Place.X, jatekos.Place.Y, jatekos.Width, jatekos.Height);
                if (jatekosrect.IntersectsWith(lovedekrect))
                {
                    jatekos.Alive = false;
                   

                }
                foreach (Wall item in wallist)
                {
                    Rect wallrect = new Rect(item.Place.X, item.Place.Y, item.Height, item.Height);
                    if (lovedekrect.IntersectsWith(wallrect))
                    {

                        lovedek = new Pisztoly(place);
                    }
                }
                
                return 1;
                
            }

            else if (jatekos.Place.X>place.X && jatekos.Place.X<= place.X+500 && merre==false && jatekos.Place.Y <= place.Y + 10 && jatekos.Place.Y <= place.Y + 10)
            {
                
                Rect lovedekrect = new Rect(lovedek.Helyzet.X, lovedek.Helyzet.Y, 30, 2);

                Rect jatekosrect = new Rect(jatekos.Place.X, jatekos.Place.Y, jatekos.Width, jatekos.Height);
                if (jatekosrect.IntersectsWith(lovedekrect))
                {
                    jatekos.Alive = false;
                    


                }
                foreach (Wall item in wallist)
                {
                    Rect wallrect = new Rect(item.Place.X, item.Place.Y, item.Height, item.Height);
                    if (lovedekrect.IntersectsWith(wallrect))
                    {

                        lovedek = new Pisztoly(place);
                    }
                }


                return 2;

            }
            else return 3;
            
        }
    }
}
