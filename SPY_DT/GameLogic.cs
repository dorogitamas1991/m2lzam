﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace SPY_DT
{
    class GameLogic
    {
        Size jatekter;
        List<Wall> walls;
        Character player;
        DispatcherTimer jumptimer;
        DispatcherTimer movetimerjobb;
        DispatcherTimer movetimerbal;
        DispatcherTimer Lovestimer;
        bool merrenez; //merre néz a karakter
        List<Pisztoly> lovedekek;

        List<Guard> guards;
        public GameLogic(string mapfilename, Size jatekter)
        {
            Merrenez = true;
            Jumptimer = new DispatcherTimer();
            Jumptimer.Interval = TimeSpan.FromMilliseconds(20);
            Jumptimer.Tick += Jump_Tick;

            movetimerjobb = new DispatcherTimer();
            movetimerjobb.Interval = TimeSpan.FromMilliseconds(20);
            movetimerjobb.Tick += Move_TickJobb;

            Movetimerbal = new DispatcherTimer();
            Movetimerbal.Interval = TimeSpan.FromMilliseconds(20);
            Movetimerbal.Tick += Move_TickBal;

            Lovestimer1 = new DispatcherTimer();
            Lovestimer1.Interval = TimeSpan.FromMilliseconds(20);
            Lovestimer1.Tick += Loves_Tick;

            this.jatekter = jatekter;
            string[] lines =
               File.ReadAllLines(mapfilename);
            walls = new List<Wall>();
            player = new Character(jatekter);
            guards = new List<Guard>();
            Lovedekek = new List<Pisztoly>();

            int xsize = int.Parse(lines[0]);
            int ysize = int.Parse(lines[1]);

            Size wallsize = new Size(jatekter.Width / 81, jatekter.Width / 81);

            for (int x = 0; x < xsize; x++)
            {
                for (int y = 0; y < ysize; y++)
                {
                    Wall wall;
                    Guard guard;
                    if (lines[y + 2][x] == 'w')
                    {
                        wall = new Wall(new Point(x * (int)wallsize.Width, y * (int)wallsize.Height), (int)wallsize.Width, (int)wallsize.Height);
                        walls.Add(wall);
                        wall.Miféle = 1;
                    }
                    else if (lines[y + 2][x] == 'f')
                    {
                        wall = new Wall(new Point(x * (int)wallsize.Width, y * (int)wallsize.Height), (int)wallsize.Width, (int)wallsize.Height);
                        walls.Add(wall);
                        wall.Miféle = 2;
                    }
                    else if (lines[y + 2][x] == 'e')
                    {
                        wall = new Wall(new Point(x * (int)wallsize.Width, y * (int)wallsize.Height), (int)wallsize.Width, (int)wallsize.Height);
                        walls.Add(wall);
                        wall.Miféle = 6;
                    }
                    else if (lines[y + 2][x] == 't')
                    {
                        wall = new Wall(new Point(x * (int)wallsize.Width, y * (int)wallsize.Height), (int)wallsize.Width, (int)wallsize.Height);
                        walls.Add(wall);
                        wall.Miféle = 3;
                    }
                    else if (lines[y + 2][x] == 'l')
                    {
                        wall = new Wall(new Point(x * (int)wallsize.Width, y * (int)wallsize.Height), (int)wallsize.Width, (int)wallsize.Height);
                        walls.Add(wall);
                        wall.Miféle = 4;
                    }
                    else if (lines[y + 2][x] == 'm')
                    {
                        wall = new Wall(new Point(x * (int)wallsize.Width, y * (int)wallsize.Height), (int)wallsize.Width, (int)wallsize.Height);
                        walls.Add(wall);
                        wall.Miféle = 5;
                    }
                    else if (lines[y + 2][x] == 'c')
                    {
                        player.Place = new Point(x * (int)wallsize.Width, y * (int)wallsize.Height);
                        player.Height = (int)wallsize.Height;
                        player.Width = (int)wallsize.Width;
                        
                        
                        
                       

                    }
                    else if (lines[y + 2][x] == 'g')
                    {
                        guard = new Guard();
                        guard.Place = new Point(x * (int)wallsize.Width, y * (int)wallsize.Height);
                        guard.Height = (int)wallsize.Height;
                        guard.Width = (int)wallsize.Width;
                        guard.Lovedek = new Pisztoly(guard.Place);
                        guards.Add(guard);

                    }


                }
            }
        }
        public void Gravity()
        {
            
            Point ujpont = new Point(player.Place.X, player.Place.Y+10);
            
            if (player.Gravity(walls, ujpont) == false)
            {
                
                player.Place = ujpont;
            }
           

        }
       

        public void MozgasJobbra()
        {

           
                Point ujpont = new Point(player.Place.X+8, player.Place.Y);
                if (player.Mozgas(walls, ujpont) == false)
                {
                    player.Place = ujpont;
               
            }
            Merrenez = true;
            
        }
        public void MozgasBalra()
        {


            Point ujpont = new Point(player.Place.X - 8, player.Place.Y);
            if (player.Mozgas(walls, ujpont) == false)
            {
                player.Place = ujpont;
               
            }
            Merrenez = false;

           


        }

        public void LovesJobb()
        {

            if (lovedekek != null)
            {


                for (int i = 0; i < lovedekek.Count; i++)
                {


                    if (Merrenez == true)
                    {

                        lovedekek[i].MozgasJobb();
                        player.Loves(lovedekek[i], guards);
                    }





                    else
                    {
                        lovedekek[i].MozgasBal();
                        player.Loves(lovedekek[i], guards);
                    }
                    if (lovedekek[i].Helyzet.X >= jatekter.Width || lovedekek[i].Helyzet.X <= 0)
                    {
                        lovedekek.Remove(lovedekek[i]);
                    }

                }
                
            
            }
            

        }
       

        public void LovesBal()
        {

            if (lovedekek != null)
            {


                foreach (Pisztoly item in lovedekek)
                {
                   

                        item.MozgasBal();
                        player.Loves(item, guards);

                }
            }


        }
        public void Guardlo()
        {
            foreach (Guard item in guards)
            {
                if (item.Visszalo(player,walls) == 1 )
                {
                    item.Lo = 2;

                }
                else if (item.Visszalo(Player,walls) == 2 ) { item.Lo = 1; }
            }
           
        }

        public bool Jump()
        {
            Point jumppoint = new Point(player.Place.X, player.Place.Y - 10); 
           
                
                    if (player.Mozgas(walls, jumppoint) == false)
                    {
                        
                        player.Place = jumppoint;
                        return true;
                        
                    }
                
                
            
            return false;
        }
        int meres = 0;
        private void Jump_Tick(object sender, EventArgs e)
        {
            if (Jump() == true)
            { Jump(); meres++; }
            if (meres == 10)
            { jumptimer.Stop();meres = 0; }
           
        }
        private void Loves_Tick(object sender, EventArgs e)
        {
            
             LovesJobb(); 

            

        }

        private void Move_TickJobb(object sender, EventArgs e)
        {
            MozgasJobbra();

        }
        private void Move_TickBal(object sender, EventArgs e)
        {
            MozgasBalra();

        }




        internal Character Player { get => player; set => player = value; }




        internal List<Wall> Walls { get => walls; set => walls = value; }
        public DispatcherTimer Jumptimer { get => jumptimer; set => jumptimer = value; }
        public int Meres { get => meres;  }
        public DispatcherTimer Movetimerjobb { get => movetimerjobb; set => movetimerjobb = value; }
        public DispatcherTimer Movetimerbal { get => movetimerbal; set => movetimerbal = value; }
        internal List<Guard> Guards { get => guards; set => guards = value; }
        public DispatcherTimer Lovestimer1 { get => Lovestimer; set => Lovestimer = value; }
        internal List<Pisztoly> Lovedekek { get => lovedekek; set => lovedekek = value; }
        public bool Merrenez { get => merrenez; set => merrenez = value; }
    }

}




