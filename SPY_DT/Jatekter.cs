﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SPY_DT
{
    class Jatekter:FrameworkElement
    {
        GameLogic VM;

        

        ImageSource img1;
        ImageSource img2;
        ImageSource img3;
        ImageSource img4;
        ImageSource img5;
        ImageSource playerimg;

        public void Init(GameLogic VM)
        {
            this.VM = VM;
            img1 = new BitmapImage(new Uri("block-01.png", UriKind.Relative));
            img2 = new BitmapImage(new Uri("block-02.png", UriKind.Relative));
            img3 = new BitmapImage(new Uri("block-03.png", UriKind.Relative));
            img4 = new BitmapImage(new Uri("block-04.png", UriKind.Relative));
            img5 = new BitmapImage(new Uri("block-05.png", UriKind.Relative));

            playerimg = new BitmapImage(new Uri("stand.png", UriKind.Relative));




        }

        protected override void OnRender(DrawingContext drawingContext)
        {


            if (VM != null)
            {
                foreach (Wall item in VM.Walls)
                {
                    
                  //  drawingContext.DrawRectangle(Brushes.Red, null, new Rect(item.Place.X, item.Place.Y, item.Width, item.Height));
                    if (item.Miféle == 1)
                    { drawingContext.DrawImage(img1, new Rect(item.Place.X, item.Place.Y, item.Width, item.Height)); }
                    else if (item.Miféle == 2)
                    { drawingContext.DrawImage(img2, new Rect(item.Place.X, item.Place.Y, item.Width, item.Height)); }
                    else if (item.Miféle == 3)
                    { drawingContext.DrawImage(img3, new Rect(item.Place.X, item.Place.Y, item.Width, item.Height)); }
                    else if (item.Miféle == 4)
                    { drawingContext.DrawImage(img4, new Rect(item.Place.X, item.Place.Y, item.Width, item.Height)); }
                    else if (item.Miféle == 5)
                    { drawingContext.DrawImage(img5, new Rect(item.Place.X, item.Place.Y, item.Width, item.Height)); }
                    else if (item.Miféle == 6)
                    { drawingContext.DrawEllipse(Brushes.BlueViolet, new Pen(Brushes.DarkBlue, 6), new Point(item.Place.X, item.Place.Y-10), 15, 30); }
                }
                foreach (Guard item in VM.Guards)
                {
                    drawingContext.DrawImage(playerimg, new Rect(item.Place.X, item.Place.Y-25, item.Width+25, item.Height+25));
                    if (item.Lo == 1 || item.Lo == 2)
                    {
                        drawingContext.DrawRectangle(Brushes.Green, null, new Rect(item.Lovedek.Helyzet.X, item.Lovedek.Helyzet.Y, item.Lovedek.Width, item.Lovedek.Height));
                    }
                }
                // drawingContext.DrawImage(playerimg, new Rect(VM.Player.Place.X, VM.Player.Place.Y, VM.Player.Width, VM.Player.Height));


                // drawingContext.DrawRectangle(Brushes.SeaGreen, null, new Rect(VM.Player.Place.X, VM.Player.Place.Y+3, VM.Player.Width, VM.Player.Height-5));
                drawingContext.DrawImage(playerimg, new Rect(VM.Player.Place.X, VM.Player.Place.Y-25, VM.Player.Width+25, VM.Player.Height+25));
                foreach (Pisztoly item in VM.Lovedekek)
                {
                    drawingContext.DrawRectangle(Brushes.Green, null, new Rect(item.Helyzet.X,item.Helyzet.Y, item.Width, item.Height));
                }

                
                   
                
        
            }
        }
    }

}
