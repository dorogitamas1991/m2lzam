﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SPY_DT
{
    class Wall:Palyaelem
    {
        Point place;
        int width;
        int height;
        int miféle;

        public Wall(Point place,int width,int height)
        {
            this.place = place;
            this.width = width;
            this.height = height;
        }

        public Point Place { get => place; set => place = value; }
        public int Width { get => width; set => width = value; }
        public int Height { get => height; set => height = value; }
        public int Miféle { get => miféle; set => miféle = value; }
    }
}
