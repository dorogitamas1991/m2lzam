﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SPY_DT
{
    class Character
    {
        Point place;
        int width;
        int height;
        Size jatekter;
        bool alive;
        
        
        

        public Point Place { get => place; set => place = value; }
        public int Width { get => width; set => width = value; }
        public int Height { get => height; set => height = value; }
        public bool Alive { get => alive; set => alive = value; }

        public Character(Size jatekter)
        {
            this.jatekter = jatekter;
            Width = (int)jatekter.Width / 85;
            Height = Width;
            alive = true;
            
            
            
            
        }
        public bool Gravity(List<Wall> walls,Point ujplace)
        {
            Rect gravityract = new Rect(place.X, place.Y+3, width, height+5);
            foreach (Wall item in walls)
            {
                Rect wallrect = new Rect(item.Place.X, item.Place.Y, item.Width, item.Height);
                if (gravityract.IntersectsWith(wallrect))
                {
                    return true;
                }


            }
            return false;

        }

        public bool Mozgas(List<Wall> walls,Point ujplace)
        {
            Rect playerrect = new Rect(ujplace.X, ujplace.Y+5, width, height-10);
            
            foreach (Wall item in walls)
            {
                Rect wallrect = new Rect(item.Place.X, item.Place.Y, item.Width, item.Height);
               if (playerrect.IntersectsWith(wallrect))
                {
                    if (playerrect.Y + playerrect.Height>= wallrect.Y)
                    {
                        return true;
                    }
                   
               }
               
               
            }
            return false;
        }
        

        public void Loves(Pisztoly lovedek,List<Guard> guards)
        {
            
               
            
                Rect lovedekrect = new Rect(lovedek.Helyzet.X, lovedek.Helyzet.Y, 30, 2);
            for (int i = 0; i < guards.Count; i++)
            {
                Rect guard = new Rect(guards[i].Place.X, guards[i].Place.Y, guards[i].Width, guards[i].Height);
                if (guard.IntersectsWith(lovedekrect))
                {
                    //pontot itt majd
                    guards.Remove(guards[i]);

                }

               
            }
           
                
                    
                
            
                

            
        }

    }
}
